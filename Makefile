DC=docker-compose
DATABASE_CONTAINER=database
PHP = php
EXEC=$(DC) exec
EXEC_PHP=$(DC) exec $(PHP)
CON = $(PHP) bin/console
AWK := $(shell command -v awk 2> /dev/null)

.DEFAULT_GOAL := help

help: ## Show this help
ifndef AWK
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'
else
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
endif

##
## Project setup
##---------------------------------------------------------------------------

install: ## Process all step in order to setup the projects
install: up vendor-install db-reset

update: ## Update the project
update: up vendor-install db-migrate

vendor-install:
	$(EXEC_PHP) composer install --no-suggest --no-progress

vendor-update:
	$(EXEC_PHP) composer update

##
## Database
##---------------------------------------------------------------------------

db-reset: ## Reset database
db-reset: db-drop db-create db-migrate

db-create: ## Create database
	$(EXEC_PHP) $(CON) doctrine:database:create --if-not-exists

db-drop: ## Drop database
	$(EXEC_PHP) $(CON) doctrine:database:drop --force --if-exists

db-gen-migration: ## Drop database
	$(EXEC_PHP) $(CON) doctrine:migration:diff

db-make-migration: ## Migrate database schema to the latest available version
	$(EXEC_PHP) $(CON) make:migration -n

db-migrate: ## Migrate database schema to the latest available version
	$(EXEC_PHP) $(CON) doctrine:migrations:migrate -n

db-migrate-validate:
	$(EXEC_PHP) -- $(CON) doctrine:migrations:version --add --all -n

db-schema-validate: ## Validate the mapping files
	$(EXEC_PHP) $(CON) doctrine:schema:validate

db-schema-drop: ## Executes (or dumps) the SQL needed to drop the current database schema
	$(EXEC_PHP) $(CON) doctrine:schema:drop --force

db-schema-update: ## Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata
	$(EXEC_PHP) $(CON) doctrine:schema:update --force

db-fixtures: ## Reset the database fixtures
	$(EXEC_PHP) $(CON) hautelook:fixtures:load --purge-with-truncate -q


fixtures:
	$(EXEC_PHP) $(CON) doctrine:fixtures:load --no-interaction

##
## TESTS
##

unit-tests: ## Run unit tests
	$(EXEC_PHP) php bin/phpunit


##
## Tools
##---------------------------------------------------------------------------

cc: ## Clear and warm up the cache in dev env
cc:
	$(EXEC_PHP) $(CON) cache:clear --no-warmup
	$(EXEC_PHP) $(CON) cache:warmup

up: 
	-$(DC) up -d

clear:  ## Remove everything: the cache, the logs, the sessions
clear: clear-files down

clear-files: perm
	-$(EXEC_PHP) rm -rf var/cache/*
	-$(EXEC_PHP) rm -rf var/sessions/*
	-$(EXEC_PHP) rm -rf var/logs/*

down: ## Stops, remove the containers and their volumes
down: 
	$(DC) down -v --remove-orphans

perm: 
	-$(EXEC_PHP) chmod -R u+rwX,go+rX,go-w var


bash: ## Access the api container via shell
	$(DC) exec $(CONTAINER)  sh


mysql-bash: ## Access the database container via shell
	$(DC) exec $(DATABASE_CONTAINER) bash


remove-migrations: 
	rm -f src/Migrations/*
	
reset-db: db-drop db-create db-migrate

diff-db: db-make-migration 

init: db-create db-migrate