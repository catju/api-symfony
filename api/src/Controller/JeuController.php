<?php 
namespace App\Controller;

// use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\Jeu;
use App\Manager\JeuManager;

#[AsController]
class JeuController extends AbstractController
{
    /**
     * @var JeuManager
     */
    protected $jeuManager;

    /**
     * Constructor.
     */
    public function __construct(JeuManager $jeuManager)
    {
        $this->jeuManager = $jeuManager;
    }

    /**
     * Récupère les informations d'un jeu
     */
    public function getJeu(Jeu $jeu, Request $request)
    {   
        return $this->json($jeu->jsonSerialize(), Response::HTTP_OK, [], ['groups' => ['id', 'jeu']]);
    } 

    /**
     * Supprime un jeu
     */
    public function deleteJeu(Jeu $jeu)
    {
        $id = $jeu->getId();
        $this->jeuManager->deleteJeu($jeu);
        $result = "Le jeu '".$id."' a été supprimé.";
        return $this->json($result, Response::HTTP_OK, [], ['groups' => ['study']]);
    }

    /**
     * Joue un jeu : permet de tester une combinaison récupéré dans la requête
     */
    public function jouerJeu(Jeu $jeu, Request $request)
    {
        // intialisation d'un message d'erreur
        $error = "'combinaison' est un paramètre requis de la requête, il ne peut pas être null.";
        $parameters = json_decode($request->getContent(), true);
                
        if(!in_array("combinaison", $parameters))
        {
            $combiToCheck = $parameters["combinaison"];           
            $result = array(
                'result' => $jeu->verifyCombinaison($combiToCheck),
                "iterations" => $jeu->getIteration(),
                "resolu" => $jeu->getResolu()
            );
            $this->jeuManager->saveJeu($jeu);
            return new JsonResponse($result, Response::HTTP_OK);
        }        
        return new JsonResponse(array("msg" => $error, "code"=>500));
    }
    
    /**
     * Création d'un jeu
     * 
     */
    public function createJeu(Request $request)
    {   

        $jeu = $this->jeuManager->newJeu();
        return $this->json($jeu->jsonSerialize(false), Response::HTTP_OK, [], ['groups' => ['id', 'jeu']]);
    } 


}