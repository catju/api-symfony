<?php

namespace App\Entity;

use App\Repository\JeuRepository;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;

use App\Controller\JeuController;

/**
 * Cette classe implémente un jeu.
 * @ORM\Entity(repositoryClass=JeuRepository::class)
 */
class Jeu
{
    public const DEFAULT_COLORS = array("rouge", "vert", "bleu", "jaune", "orange");
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Solution du jeu
     * @ORM\Column(type="array")
     */
    private $combinaison = array();

    /**
     * Nombre d'itération du jeu
     * @ORM\Column(type="integer")
     */
    private $iteration;

    /**
     * Flag pour savoir si le jeu est résolu
     * @ORM\Column(type="boolean")
     */
    private $resolu;

    /**
     * Constructeur.
     * @var int $nbColors : nombre de couleurs à trouver dans la combinaison.
     */
    public function __construct($nbColors=5)
    {
        $this->resolu = false;
        $this->iteration = 0;
        $this->initCombinaison($nbColors);
    }

    /**
     * Initialise la combinaison.
     * @var int $nbColors : nombre de couleurs à trouver dans la combinaison.
     */
    private function initCombinaison($nbColors)
    {
        $this->combinaison = array();
        for ($i=0; $i < $nbColors; $i++) { 
            $idxRandom = array_rand(self::DEFAULT_COLORS, 1);
            $this->combinaison[] = self::DEFAULT_COLORS[$idxRandom];
        }
    }

    /**
     * Vérifie si la combinaison donnée est correcte.
     * Elle renvoie un tableau comprenant pour chaque cellule : 
     *   -	0 : si la couleur n’existe pas dans la combinaison, ou qu’elle existe mais est déjà bien placée
     *   -	1 : si la couleur n’est pas correcte, qu’elle existe dans la combinaison et n’est pas encore bien placée
     *   -	2 : si la couleur est correcte
     * @var array $combi : la combinaison à tester
     */
    public function verifyCombinaison(array $combi)
    {
        $result = array();
        $toCheck = $this->combinaison;
        foreach ($combi as $index => $couleur) {
            $value = 0;
            if($this->resolu) {
                $value = 2;
            } else if(in_array($couleur, $toCheck)){
                // la couleur est dans la combinaison
                if($couleur == $toCheck[$index]) {
                    // la couleur est à la bonne place
                    $value = 2;
                    $toCheck[$index] = null;
                } else {
                    // il faut vérifier qu'il ne s'agit pas d'une couleur bien placé dans une autre case de $combi
                    foreach($toCheck as $idxToCheck => $c)
                    {
                        if($couleur == $c && $combi[$idxToCheck] != $couleur) {
                            $value = 1;
                            $toCheck[$idxToCheck] = null;
                            break;
                        }
                    }
                }                
            }
            
            $result[] = $value;
        }
        // ajout une itération
        $this->addIteration();
        // vérifie si le jeu est résolu        
        if($combi == $this->combinaison) {    
            $this->resolu = true;
        }
        return $result;
    }

    /**
     * Gets the id.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Gets iteration.
     */
    public function getIteration(): ?int
    {
        return $this->iteration;
    }

    /**
     * Sets iteration.
     */
    public function addIteration(): self
    {
        if(!$this->resolu) $this->iteration += 1;
        return $this;
    }

    /**
     * Gets the flag to know if the game is finished.
     */
    public function getResolu(): ?bool
    {
        return $this->resolu;
    }

    /**
     * Sets the game to finish.
     */
    public function resolu(): self
    {
        $this->resolu = true;
        return $this;        
    }

    public function getCombinaison(): array 
    {
        return $this->combinaison;
    }

    public function getNbColors(): int
    {
        return count($this->combinaison);
    }

    /**
     */
    public function jsonSerialize($show_combinaison=true)
    {
        $data = array(
            'id'   => $this->id,
            'resolu' => $this->resolu,
            'iteration' => $this->iteration
        );
        if($show_combinaison) $data["combinaison"] = $this->combinaison;
        if(is_null($data["id"])) unset($data["id"]);
        return $data;
    }

}
