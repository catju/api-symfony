<?php

namespace App\Manager;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Jeu;
use App\Repository\JeuRepository;

class JeuManager
{
    private $em;
    private $jeuRepository;
    
    public function __construct(EntityManagerInterface $em, JeuRepository $jeuRepository)
    {
        $this->em = $em;
        $this->jeuRepository = $jeuRepository;
    }

    public function newJeu()
    {
        $jeu = new Jeu();
        $this->em->persist($jeu);
        $this->em->flush();

        return $jeu;
    }

    public function deleteJeu(Jeu $jeu)
    {
        $this->em->remove($jeu);
        $this->em->flush();
    }

    public function saveJeu(Jeu $jeu)
    {
        $this->em->persist($jeu);
        $this->em->flush();

        return $jeu;
    }

}